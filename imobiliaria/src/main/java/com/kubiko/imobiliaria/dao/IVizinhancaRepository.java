/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.dao;

import com.kubiko.imobiliaria.models.Publicacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author elton
 */
@Repository
public interface IVizinhancaRepository extends CrudRepository<Publicacao, Long> {

}
