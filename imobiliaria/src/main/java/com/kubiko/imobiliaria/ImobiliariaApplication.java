package com.kubiko.imobiliaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableAsync
//@EnableJpaAuditing(auditorAwareRef="auditorAware")
@EnableJpaRepositories(basePackages = {"com.kubiko"})
//@EntityScan(basePackages = {"com.kubiko"})
//@ComponentScan(basePackages = {"com.kubiko"})
//@EnableTransactionManagement
public class ImobiliariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImobiliariaApplication.class, args);
	}
         
//    @Bean
//    public AuditorAware<String> auditorAware() {
//        return new SpringSecurityAuditorAware();
//    }
     
}
