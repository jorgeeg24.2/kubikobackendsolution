/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.basics.AuditModel;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Resolution;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author elton
 */
@Indexed
@Entity
@Table(name = "IMOB_PUBLICACAO")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Publicacao extends AuditModel {
    
    private static final long serialVersionUID = 6529685098267757690L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(nullable = false, length=45)
    @NotEmpty(message="O nome do dono da agencia não foi preenchido")
    @Size(max = 45, message="O nome do dono da agencia deverá conter no máximo 45 caracteres")
    private String agenciaDono;
    
    @Column(nullable = false, length=45)
    @NotEmpty(message="O acesso não foi preenchido")
    @Size(max = 45, message="O acesso deverá conter no máximo 45 caracteres")
    private String acesso;
    
    @Column(nullable = false, length=45)
    @NotEmpty(message="A aprovação não foi preenchido")
    @Size(max = 45, message="A aprovação deverá conter no máximo 45 caracteres")
    private String aprovacao;
    
    @Column(nullable = false, length=45)
    @NotEmpty(message="A publicaçao não foi preenchido")
    @Size(max = 45, message="A publicaçao deverá conter no máximo 45 caracteres")
    private String publicacao;
    
    @NotNull
    @DateBridge(resolution = Resolution.DAY)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date dataInicio;
    
    @NotNull
    @DateBridge(resolution = Resolution.DAY)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date dataFim;
       
    @NotNull
    @DateBridge(resolution = Resolution.DAY)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date dataCriacao;

    @IndexedEmbedded
    @ManyToMany
//        @JoinTable(
//        name = "IMOB_PROPR_PUB", 
//        joinColumns = @JoinColumn(name = "propriedade_id"), 
//        inverseJoinColumns = @JoinColumn(name = "publicacao_id"))
    private Set<Propriedade> propriedade;
        
    @IndexedEmbedded
    @ManyToOne
    @JoinColumn
    private DonoPropriedade donoProprie;

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getAgenciaDono() {
        return agenciaDono;
    }

    public void setAgenciaDono(String agenciaDono) {
        this.agenciaDono = agenciaDono;
    }

    public String getAcesso() {
        return acesso;
    }

    public void setAcesso(String acesso) {
        this.acesso = acesso;
    }

    public String getAprovacao() {
        return aprovacao;
    }

    public void setAprovacao(String aprovacao) {
        this.aprovacao = aprovacao;
    }

    public String getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(String publicacao) {
        this.publicacao = publicacao;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Set<Propriedade> getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(Set<Propriedade> propriedade) {
        this.propriedade = propriedade;
    }

    public DonoPropriedade getDonoProprie() {
        return donoProprie;
    }

    public void setDonoProprie(DonoPropriedade donoProprie) {
        this.donoProprie = donoProprie;
    }

}
