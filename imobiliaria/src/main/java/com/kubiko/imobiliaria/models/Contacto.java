/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.models.TipoContactoBase;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author elton
 */
@Indexed
@Entity
@Table(name = "IMOB_CONTACTO")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Contacto extends TipoContactoBase{
    
    @IndexedEmbedded
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private DonoPropriedade donoProprie;

    public DonoPropriedade getDonoProprie() {
        return donoProprie;
    }

    public void setDonoProprie(DonoPropriedade donoProprie) {
        this.donoProprie = donoProprie;
    }
}
