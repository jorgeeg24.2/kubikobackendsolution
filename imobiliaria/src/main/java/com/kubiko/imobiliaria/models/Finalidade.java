/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoAluguer;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoCompra;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoPreco;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.ContainedIn;

/**
 *
 * @author elton
 */

@Entity
@Table(name = "IMOB_FINALIDADE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Finalidade {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(nullable = false, length=45)
    @NotEmpty(message="A finalidadecol não foi preenchido")
    @Size(max = 45, message="A finalidadecol deverá conter no máximo 45 caracteres")
    private String finalidadecol;
    
    @ManyToOne
    @JoinColumn
//    @JsonIgnore
    private Propriedade propriedade;
    
    @ContainedIn
    @OneToMany(mappedBy = "finalidade")
//    @JsonIgnore
    private Set<TipoCompra> tipCompra;

    @ContainedIn
    @OneToMany(mappedBy = "finalidade")
//    @JsonIgnore
    private Set<TipoAluguer> tipAluguer;

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getFinalidadecol() {
        return finalidadecol;
    }

    public void setFinalidadecol(String finalidadecol) {
        this.finalidadecol = finalidadecol;
    }

    public Propriedade getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(Propriedade propriedade) {
        this.propriedade = propriedade;
    }

    public Set<TipoCompra> getTipCompra() {
        return tipCompra;
    }

    public void setTipCompra(Set<TipoCompra> tipCompra) {
        this.tipCompra = tipCompra;
    }

    public Set<TipoAluguer> getTipAluguer() {
        return tipAluguer;
    }

    public void setTipAluguer(Set<TipoAluguer> tipAluguer) {
        this.tipAluguer = tipAluguer;
    }
    
}
