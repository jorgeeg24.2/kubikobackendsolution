/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models.parametrizaveis;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.parametrizaveis.TipoCompraBase;
import com.kubiko.imobiliaria.models.Finalidade;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author elton
 */
@Indexed
@Entity
@Table(name = "IMOB_TIPO_COMPRA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TipoCompra extends TipoCompraBase {

    @Column(nullable = false, length=45)
    @NotEmpty(message="O nome da compra do Imobiliario não foi preenchido")
    @Size(max = 45, message="O nome da compra deverá conter no máximo 100 caracteres")
    private String nomeCompra;

    @IndexedEmbedded
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Finalidade finalidade;

    public String getNomeCompra() {
        return nomeCompra;
    }

    public void setNomeCompra(String nomeCompra) {
        this.nomeCompra = nomeCompra;
    }

    public Finalidade getFinalidade() {
        return finalidade;
    }

    public void setFinalidade(Finalidade finalidade) {
        this.finalidade = finalidade;
    }

    public String getTipoObjecto() {
        return tipoObjecto;
    }

    public void setTipoObjecto(String tipoObjecto) {
        this.tipoObjecto = tipoObjecto;
    }

}
