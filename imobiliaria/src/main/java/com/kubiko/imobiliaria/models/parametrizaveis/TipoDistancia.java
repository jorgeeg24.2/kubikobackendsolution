/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models.parametrizaveis;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.parametrizaveis.TipoDistanciaBase;
import com.kubiko.imobiliaria.models.Vizinhanca;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.search.annotations.Indexed;

/**
 *
 * @author elton
 */

@Entity
@Table(name = "IMOB_TIPO_DISTANCIA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TipoDistancia extends TipoDistanciaBase {
    
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Vizinhanca vizinhanca;

    public Vizinhanca getVizinhanca() {
        return vizinhanca;
    }

    public void setVizinhanca(Vizinhanca vizinhanca) {
        this.vizinhanca = vizinhanca;
    }

    public String getTipoObjecto() {
        return tipoObjecto;
    }

    public void setTipoObjecto(String tipoObjecto) {
        this.tipoObjecto = tipoObjecto;
    }   
}
