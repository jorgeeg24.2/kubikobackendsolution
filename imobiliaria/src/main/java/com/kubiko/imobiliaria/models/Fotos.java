/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import static org.apache.kafka.common.protocol.types.ArrayOf.nullable;

/**
 *
 * @author elton
 */

@Entity
@Table(name = "IMOB_FOTOS")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Fotos implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(nullable = false, length=45)
    @NotEmpty(message="O nome não foi preenchido")
    @Size(max = 45, message="O nome deverá conter no máximo 45 caracteres")
    private String nomeFots;

    @Column(length=45)
//    @NotEmpty(message="A descrição não foi preenchido")
    @Size(max = 45, message="A descrição deverá conter no máximo 45 caracteres")
    private String descricaoFots;
    
    @NotNull
    @Column(length=7)
    private String tamanho;
    
    @Column(length=15)
    private String tipo;

    @ManyToOne
    @JoinColumn(nullable = false)
//    @MapsId("id")
    private Propriedade propriedade;
    
    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getNomeFots() {
        return nomeFots;
    }

    public void setNomeFots(String nomeFots) {
        this.nomeFots = nomeFots;
    }

    public String getDescricaoFots() {
        return descricaoFots;
    }

    public void setDescricaoFots(String descricaoFots) {
        this.descricaoFots = descricaoFots;
    }

    public Propriedade getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(Propriedade propriedade) {
        this.propriedade = propriedade;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
        
}
