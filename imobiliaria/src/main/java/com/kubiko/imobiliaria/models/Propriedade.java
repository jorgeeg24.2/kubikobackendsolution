/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoPreco;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoPropriedade;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author elton
 */
@Indexed
@Entity
@Table(name = "IMOB_PROPRIEDADE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Propriedade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(nullable = false, length=45)
    @NotEmpty(message="O nome da propriedade não foi preenchido")
    @Size(max = 45, message="O nome da propriedade deverá conter no máximo 45 caracteres")
    private String nomePropried;

    @Column(length = 100)
//    @NotEmpty(message="A descrição não foi preenchido")
    @Size(max = 100, message="A descrição da propriedade deverá conter no máximo 100 caracteres")
    private String descrPropried;
    
    @Column(nullable = false, length=45)
    @NotEmpty(message="A propriedadecol não foi preenchido")
    @Size(max = 45, message="A propriedadecol deverá conter no máximo 45 caracteres")
    private String Propriedadecol;

    @Column(nullable = false, length=45)
    @NotEmpty(message="A referençia da propriedade não foi preenchido")
    @Size(max = 45, message="A referençia da propriedade deverá conter no máximo 45 caracteres")
    private String referencia;

    @Column(length=45)
//    @NotEmpty(message="O tamanho da casa não foi preenchido")
    @Size(max = 45, message="O tamanho da casa deverá conter no máximo 45 caracteres")
    private String tamanhoCasa;    
    
    @NotNull
    private BigDecimal longitude;

    @NotNull
    private BigDecimal latitude;
    
    private Long numQuartos;
    
    private Long numWc;
    
    @NotNull
    private Long anoContrucao;
    
    @NotNull
    private Long tamTerreno;
  
    private Long tamAreaVerde;
    
    @IndexedEmbedded
    @ManyToOne
    @JoinColumn
//    @MapsId("id")
    private Localizacao localizacao;
    
    @ContainedIn
    @OneToMany(mappedBy = "propriedade")
    private Set<TipoPropriedade> tipoProp;
    
    @OneToMany(mappedBy = "propriedade")
    private Set<Fotos> fotos;

    @OneToMany(mappedBy = "propriedade")
    private Set<Finalidade> finalidade;

    @ContainedIn
    @OneToMany(mappedBy = "propriedade")
    private Set<TipoPreco> tipoPreco;

    @IndexedEmbedded
    @ManyToMany
    @JsonIgnore
    private Set<Vizinhanca> vizinhanca;

    @ContainedIn
    @ManyToMany(mappedBy = "propriedade")
    private Set<Publicacao> publicacao;

    @IndexedEmbedded
    @ManyToMany
    private Set<Conveniencia> conven;

    public String getNomePropried() {
        return nomePropried;
    }

    public void setNomePropried(String nomePropried) {
        this.nomePropried = nomePropried;
    }

    public String getDescrPropried() {
        return descrPropried;
    }

    public void setDescrPropried(String descrPropried) {
        this.descrPropried = descrPropried;
    }

    public String getPropriedadecol() {
        return Propriedadecol;
    }

    public void setPropriedadecol(String Propriedadecol) {
        this.Propriedadecol = Propriedadecol;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public String getTamanhoCasa() {
        return tamanhoCasa;
    }

    public void setTamanhoCasa(String tamanhoCasa) {
        this.tamanhoCasa = tamanhoCasa;
    }

    public Long getNumQuartos() {
        return numQuartos;
    }

    public void setNumQuartos(Long numQuartos) {
        this.numQuartos = numQuartos;
    }

    public Long getNumWc() {
        return numWc;
    }

    public void setNumWc(Long numWc) {
        this.numWc = numWc;
    }

    public Long getAnoContrucao() {
        return anoContrucao;
    }

    public void setAnoContrucao(Long anoContrucao) {
        this.anoContrucao = anoContrucao;
    }

    public Long getTamTerreno() {
        return tamTerreno;
    }

    public void setTamTerreno(Long tamTerreno) {
        this.tamTerreno = tamTerreno;
    }

    public Long getTamAreaVerde() {
        return tamAreaVerde;
    }

    public void setTamAreaVerde(Long tamAreaVerde) {
        this.tamAreaVerde = tamAreaVerde;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public Set<TipoPreco> getTipoPreco() {
        return tipoPreco;
    }

    public void setTipoPreco(Set<TipoPreco> tipoPreco) {
        this.tipoPreco = tipoPreco;
    }

    public Set<Conveniencia> getConven() {
        return conven;
    }

    public void setConven(Set<Conveniencia> conven) {
        this.conven = conven;
    }

    public Set<Fotos> getFotos() {
        return fotos;
    }

    public void setFotos(Set<Fotos> fotos) {
        this.fotos = fotos;
    }

    public Set<Finalidade> getFinalidade() {
        return finalidade;
    }

    public void setFinalidade(Set<Finalidade> finalidade) {
        this.finalidade = finalidade;
    }

    public Set<Vizinhanca> getVizinhanca() {
        return vizinhanca;
    }

    public void setVizinhanca(Set<Vizinhanca> vizinhanca) {
        this.vizinhanca = vizinhanca;
    }

    public Set<Publicacao> getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(Set<Publicacao> publicacao) {
        this.publicacao = publicacao;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Set<TipoPropriedade> getTipoProp() {
        return tipoProp;
    }

    public void setTipoProp(Set<TipoPropriedade> tipoProp) {
        this.tipoProp = tipoProp;
    }

}
