/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.ContainedIn;

/**
 *
 * @author elton
 */

@Entity
@Table(name = "IMOB_DONO_PROPRIE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DonoPropriedade implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Column(length=45)
    @NotEmpty(message="O nome do dono da propriedade não foi preenchido")
    @Size(max = 45, message="O nome do dono da propriedade deverá conter no máximo 45 caracteres")
    private String nome;
    
    @ContainedIn
    @OneToMany(mappedBy = "donoProprie", cascade  = CascadeType.ALL, orphanRemoval = true)
    private Set<Contacto> contacto;
     
    @ContainedIn
    @OneToMany(mappedBy = "donoProprie", cascade  = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private Set<Publicacao> publicacao;

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Set<Contacto> getContacto() {
        return contacto;
    }

    public void setContacto(Set<Contacto> contacto) {
        this.contacto = contacto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<Publicacao> getPublicacao() {
        return publicacao;
    }

    public void setPublicacao(Set<Publicacao> publicacao) {
        this.publicacao = publicacao;
    }
    
   
}
