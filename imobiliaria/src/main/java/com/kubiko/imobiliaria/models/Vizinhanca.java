/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoDistancia;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoVizinhanca;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author elton
 */
@Entity
@Table(name = "IMOB_VIZINHANCA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Vizinhanca implements Serializable {
                
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    
    @Column(length=45)
//    @NotEmpty(message="A distançia da vizinhança não foi preenchido")
    @Size(max = 45, message="A distançia da vizinhança deverá conter no máximo 45 caracteres")
    private String distancia;

    @OneToMany(mappedBy = "vizinhanca", cascade = CascadeType.ALL, orphanRemoval=true)
    private Set<TipoVizinhanca> tipoVizinh;
    
    @ContainedIn
    @ManyToMany(mappedBy="vizinhanca")
    @JsonIgnore
    private Set<Propriedade> propriedade;
    
    @OneToMany(mappedBy = "vizinhanca")
    private Set <TipoDistancia> tipoDist;

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public Set<TipoVizinhanca> getTipoVizinh() {
        return tipoVizinh;
    }

    public void setTipoVizinh(Set<TipoVizinhanca> tipoVizinh) {
        this.tipoVizinh = tipoVizinh;
    }

    public Set<Propriedade> getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(Set<Propriedade> propriedade) {
        this.propriedade = propriedade;
    }

    public Set<TipoDistancia> getTipoDist() {
        return tipoDist;
    }

    public void setTipoDist(Set<TipoDistancia> tipoDist) {
        this.tipoDist = tipoDist;
    }

}
