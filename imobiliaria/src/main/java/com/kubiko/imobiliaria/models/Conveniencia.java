/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.imobiliaria.models.parametrizaveis.TipoConveniencia;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author elton
 */

@Entity
@Table(name = "IMOB_CONVENIENCIA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Conveniencia implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @ContainedIn
    @OneToMany(mappedBy = "conveniencia")
    private Set<TipoConveniencia> tipoConv;

    @ContainedIn
    @ManyToMany(mappedBy="conven")    
    @JsonIgnore
    private Set<Propriedade> propriedade;
      
    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public Set<TipoConveniencia> getTipoConv() {
        return tipoConv;
    }

    public void setTipoConv(Set<TipoConveniencia> tipoConv) {
        this.tipoConv = tipoConv;
    }   

    public Set<Propriedade> getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(Set<Propriedade> propriedade) {
        this.propriedade = propriedade;
    }
}
