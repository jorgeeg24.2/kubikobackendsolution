/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models.parametrizaveis;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.parametrizaveis.TipoAluguerBase;
import com.kubiko.imobiliaria.models.Finalidade;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author elton
 */

@Indexed
@Entity
@Table(name = "IMOB_TIPO_ALUGUER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TipoAluguer extends TipoAluguerBase {
  
    @IndexedEmbedded
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Finalidade finalidade;
    
    @OneToMany(mappedBy = "tipoAluguer")
    private Set<TipoCasa> tipoCasa;

    public Finalidade getFinalidade() {
        return finalidade;
    }

    public void setFinalidade(Finalidade finalidade) {
        this.finalidade = finalidade;
    }

    public String getTipoObjecto() {
        return tipoObjecto;
    }

    public void setTipoObjecto(String tipoObjecto) {
        this.tipoObjecto = tipoObjecto;
    }

    public Set<TipoCasa> getTipoCasa() {
        return tipoCasa;
    }

    public void setTipoCasa(Set<TipoCasa> tipoCasa) {
        this.tipoCasa = tipoCasa;
    }
    
}
