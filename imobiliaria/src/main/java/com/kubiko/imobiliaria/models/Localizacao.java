/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.basics.AuditModel;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

/**
 *
 * @author elton
 */
@Indexed
@Entity
@Table(name = "IMOB_LOCALIZACAO")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Localizacao extends AuditModel {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @Field
    @Column(nullable = false, length=45)
    @NotEmpty(message="O nome da ilha não foi preenchido")
    @Size(max = 45, message="O nome da ilha deverá conter no máximo 45 caracteres")
    private String ilha;
    
    @Field
    @Column(nullable = false, length=45)
    @NotEmpty(message="O nome da zona não foi preenchido")
    @Size(max = 45, message="O nome da zona deverá conter no máximo 45 caracteres")
    private String zona;
    
    @Field
    @Column(nullable = false, length=45)
    @NotEmpty(message="O nome do conselho não foi preenchido")
    @Size(max = 45, message="O nome do conselho deverá conter no máximo 45 caracteres")
    private String conselho;
    
    @ContainedIn
    @OneToMany(mappedBy = "localizacao", cascade = CascadeType.ALL, orphanRemoval=true)
    @JsonIgnore
    private Set<Propriedade> propriedade;
    
    
    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getIlha() {
        return ilha;
    }

    public void setIlha(String ilha) {
        this.ilha = ilha;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getConselho() {
        return conselho;
    }

    public void setConselho(String conselho) {
        this.conselho = conselho;
    }

    public Set<Propriedade> getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(Set<Propriedade> propriedade) {
        this.propriedade = propriedade;
    }
  
}
