/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models.parametrizaveis;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.parametrizaveis.TipoCasaBase;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.search.annotations.Indexed;

/**
 *
 * @author elton
 */
@Entity
@Table(name = "IMOB_TIPO_CASA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TipoCasa extends TipoCasaBase {
  
    @NotNull(message="Atribua uma casa para Aluguer")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private TipoAluguer tipoAluguer;

    public TipoAluguer getTipoAluguer() {
        return tipoAluguer;
    }

    public void setTipoAluguer(TipoAluguer tipoAluguer) {
        this.tipoAluguer = tipoAluguer;
    }

    public String getTipoObjecto() {
        return tipoObjecto;
    }

    public void setTipoObjecto(String tipoObjecto) {
        this.tipoObjecto = tipoObjecto;
    }
}
