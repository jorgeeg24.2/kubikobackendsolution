/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.imobiliaria.models.parametrizaveis;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kubiko.common.superclass.parametrizaveis.TipoConvenienciaBase;
import com.kubiko.imobiliaria.models.Conveniencia;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

/**
 *
 * @author elton
 */
@Indexed
@Entity
@Table(name = "IMOB_TIPO_CONVEN")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TipoConveniencia extends TipoConvenienciaBase {
       
    @IndexedEmbedded
    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Conveniencia conveniencia;

    public Conveniencia getConveniencia() {
        return conveniencia;
    }

    public void setConveniencia(Conveniencia conveniencia) {
        this.conveniencia = conveniencia;
    }

    public String getTipoObjecto() {
        return tipoObjecto;
    }

    public void setTipoObjecto(String tipoObjecto) {
        this.tipoObjecto = tipoObjecto;
    }
    
}
