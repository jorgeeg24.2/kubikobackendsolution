package com.sintaxy.kubiko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RenovacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RenovacaoApplication.class, args);
	}

}
