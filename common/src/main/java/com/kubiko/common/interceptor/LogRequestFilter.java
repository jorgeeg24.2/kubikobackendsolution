///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.kubiko.common.interceptor;
//
//import com.kubiko.common.dao.IDebugRepository;
//import com.kubiko.common.models.Debug;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.io.StringWriter;
//import java.io.UnsupportedEncodingException;
//import java.security.Principal;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.Ordered;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.OncePerRequestFilter;
//import org.springframework.web.util.ContentCachingRequestWrapper;
//import org.springframework.web.util.WebUtils;
//
///**
//* A filter which logs web requests that lead to an error in the system.
//*
//*/
//@Component
//public class LogRequestFilter extends OncePerRequestFilter implements Ordered {
//
//    @Autowired
//    private IDebugRepository debugRepo;
//    @Value("${debug-request}")
//    private boolean debugRequest;
//    private static final String REQUEST_METHOD="method";
//    private static final String REQUEST_PATH="path";
//    private static final String REQUEST_STATUS="status";
//    private static final String REQUEST_USER="user";
//    private static final String REQUEST_QUERY="query";
//    private static final String REQUEST_ERROR="error";
//    private static final String REQUEST_BODY="body";
//    
//    private final Log logger = LogFactory.getLog(getClass());
//
//    // put filter at the end of all other filters to make sure we are processing after all others
//    private int order = Ordered.LOWEST_PRECEDENCE - 8;
//
//    @Override
//    public int getOrder() {
//        return order;
//    }
//
//    @Override
//    protected void doFilterInternal(
//            HttpServletRequest request,
//            HttpServletResponse response,
//            FilterChain filterChain) throws ServletException, IOException {
//        ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);
//        final String method = request.getMethod();
//        try {
//            filterChain.doFilter(wrappedRequest, response);
//            final int status = response.getStatus();
//            if(debugRequest && status==400
//                    && (method.equals("POST") || method.equals("PUT") || method.equals("PATCH"))) {
//                Map<String, Object> trace = getTrace(wrappedRequest, status, null);
//                getBody(wrappedRequest, trace);
//                logTrace(wrappedRequest, trace);
//            }
//        } catch(Exception ex){            
//            if(debugRequest && (method.equals("POST") || method.equals("PUT") || method.equals("PATCH"))) {
//                Map<String, Object> trace = getTrace(wrappedRequest, 500, ex);
//                getBody(wrappedRequest, trace);
//                logTrace(wrappedRequest, trace);
//            }
//            throw ex;
//        }
//
//    }
//
//    private void getBody(ContentCachingRequestWrapper request, Map<String, Object> trace) {
//        // wrap request to make sure we can read the body of the request (otherwise it will be consumed by the actual
//        // request handler)
//        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
//        if (wrapper != null) {
//            byte[] buf = wrapper.getContentAsByteArray();
//            if (buf.length > 0) {
//                String payload;
//                try {
//                    payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
//                }
//                catch (UnsupportedEncodingException ex) {
//                    payload = "[unknown]";
//                }
//
//                trace.put(REQUEST_BODY, payload);
//            }
//        }
//    }
//
//    private void logTrace(HttpServletRequest request, Map<String, Object> trace) {
//        Debug deb = new Debug();
//        deb.setStatus((Integer) trace.get(REQUEST_STATUS));
//        deb.setMethod((String) trace.get(REQUEST_METHOD));
//        deb.setPath((String) trace.get(REQUEST_PATH));
//        try{
//            deb.setUser((String) trace.get(REQUEST_USER));
//        }catch(Exception exception){}
//        deb.setQuery((String) trace.get(REQUEST_QUERY));
//        deb.setValor(trace.get(REQUEST_BODY));
//        if(trace.get(REQUEST_ERROR)!=null)
//            deb.setException((String) trace.get(REQUEST_ERROR));
////        deb.setClient(request.getHeader("User-Agent"));
////        deb.setIp(request.getHeader("X-FORWARDED-FOR"));
//        
//        debugRepo.save(deb);
//    }
//
//    protected Map<String, Object> getTrace(HttpServletRequest request, int status, Exception ex) {
//        Principal principal = request.getUserPrincipal();
//
//        Map<String, Object> trace = new LinkedHashMap<String, Object>();
//        trace.put(REQUEST_METHOD, request.getMethod());
//        trace.put(REQUEST_PATH, request.getRequestURI());
//        try{
//            trace.put(REQUEST_USER, principal.getName());
//        }catch(Exception exception){}
//        trace.put(REQUEST_QUERY, request.getQueryString());
//        trace.put(REQUEST_STATUS, status);
//        if(ex!=null) {
//            StringWriter errors = new StringWriter();
//            ex.printStackTrace(new PrintWriter(errors));
//            trace.put(REQUEST_ERROR, errors.toString());
//        }
//
//        return trace;
//    }
//
//}
