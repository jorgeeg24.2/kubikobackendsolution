    package com.kubiko.common.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Historico implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private Integer version;
    @NotNull
    private Date changedAt;
    @NotNull
    private String changedBy;
    @NotNull
    private String tipoObjeto;
//    @NotNull
//    @Column(length=1)
//    private Integer tipoOperacao;
    @NotNull
    private Long objetoId;
    
    @NotNull
    @JsonIgnore
    @Lob
    private String valor;    
    
//    public Integer getTipoOperacao() {
//        return tipoOperacao;
//    }
//
//    public void setTipoOperacao(Integer tipoOperacao) {
//        this.tipoOperacao = tipoOperacao;
//    }
    
    public Long getObjetoId() {
        return objetoId;
    }

    public void setObjetoId(Long objetoId) {
        this.objetoId = objetoId;
    }

    public Object getValor() {
        try {
            return (new ObjectMapper()).readValue(valor, Object.class);
        } catch (IOException ex) {
            Logger.getLogger(Historico.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Problemas ao carregar valor do histórico.");
        }
    }

    public void setValor(Object valor) {
        try {
            this.valor = (new ObjectMapper()).writeValueAsString(valor);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Historico.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Problemas ao guardar valor do histórico.");
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getChangedAt() {
        return changedAt;
    }

    public void setChangedAt(Date changedAt) {
        this.changedAt = changedAt;
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }
    
    public String getTipoObjeto() {
        return tipoObjeto;
    }

    public void setTipoObjeto(String tipoObjeto) {
        this.tipoObjeto = tipoObjeto;
    }
    
    
}
