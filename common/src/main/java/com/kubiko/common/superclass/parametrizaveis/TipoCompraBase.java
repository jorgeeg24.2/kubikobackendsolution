/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.common.superclass.parametrizaveis;

import com.kubiko.common.constants.ParamConst;
import com.kubiko.common.superclass.basics.AuditParameterModel;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.Field;

/**
 *
 * @author elton
 */
@MappedSuperclass
public class TipoCompraBase extends AuditParameterModel {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_TIPCOMP")
    private Long id;

    @Field
    @NotEmpty(message="A designação não foi preenchida")
    @Size(max = 100, message="A designação deverá conter no máximo 100 caracteres")
    private String tipo;

    public TipoCompraBase() {
        
            this.tipoObjecto=ParamConst.PARAM_TIPO_COMPRA;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoObjecto() {
        return tipoObjecto;
    }

    public void setTipoObjecto(String tipoObjecto) {
        this.tipoObjecto = tipoObjecto;
    }
    
}
