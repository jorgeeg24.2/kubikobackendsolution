/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.common.superclass.models;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.search.annotations.Field;

/**
 *
 * @author elton
 */
@MappedSuperclass
public class TipoContactoBase {     
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_TIPCONT")
    private Long id;
    
    @Field
    private BigDecimal latitude;

    @Field
    private BigDecimal Longitude;

    @Field
    @Email(message="O email não é válido")
    private String email;
    @Field
    @Email(message="O email não é válido")
    private String email2;

    @Field
    @Size(max = 120, message="A morada deverá conter no máximo 120 caracteres")
    private String morada;

    @Field
    @Size(max = 15, message="O código postal deverá conter no máximo 15 caracteres")
    private String codPostal;

    @Field
    @Size(max = 16, message="O telefone deverá conter no máximo 16 digitos")
    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\ \\./0-9]*$", message="Formato de telefone/telemovel inválido")
    private String telefone;

    @Field
    @Size(max = 16, message="O telemóvel deverá conter no máximo 16 digitos")
    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\ \\./0-9]*$", message="Formato de telefone/telemovel inválido")
    private String telemovel;
    
    @Field
    @Size(max = 16, message="O telefone deverá conter no máximo 16 digitos")
    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\ \\./0-9]*$", message="Formato de telefone/telemovel inválido")
    private String telefone2;

    @Field
    @Size(max = 16, message="O telemóvel deverá conter no máximo 16 digitos")
    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\ \\./0-9]*$", message="Formato de telefone/telemovel inválido")
    private String telemovel2;

    @Field
    @Size(max = 16, message="O fax deverá conter no máximo 16 caracteres")
    private String fax;
    @Field
    @Size(max = 16, message="O fax deverá conter no máximo 16 caracteres")
    private String fax2;

    @Field
    @Size(max = 5, message="A porta deverá conter no máximo 5 caracteres")
    private String porta;

    @Field
    @Size(max = 10, message="A morada deverá conter no máximo 10 caracteres")
    private String casa;

    @Field
    @Size(max = 10, message="O piso deverá conter no máximo 10 caracteres")
    private String piso;

    @Field
    @Column(name = "PORTAPISO")
    @Size(max = 16, message="O porta/piso deverá conter no máximo 16 caracteres")
    private String portaPiso;

    @Field
    @Column(name = "WEBURL")
    @Size(max = 100, message="O url deverá conter no máximo 100 caracteres")
    @Pattern(regexp = "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})",
            message="Formato de url inválido")
    private String webUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return Longitude;
    }

    public void setLongitude(BigDecimal Longitude) {
        this.Longitude = Longitude;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelemovel() {
        return telemovel;
    }

    public void setTelemovel(String telemovel) {
        this.telemovel = telemovel;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public String getTelemovel2() {
        return telemovel2;
    }

    public void setTelemovel2(String telemovel2) {
        this.telemovel2 = telemovel2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFax2() {
        return fax2;
    }

    public void setFax2(String fax2) {
        this.fax2 = fax2;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public String getCasa() {
        return casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getPortaPiso() {
        return portaPiso;
    }

    public void setPortaPiso(String portaPiso) {
        this.portaPiso = portaPiso;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

}
