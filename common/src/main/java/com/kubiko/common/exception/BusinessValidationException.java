/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.common.exception;

import java.util.List;

/**
 *
 * @author abel
 */
public class BusinessValidationException extends RuntimeException {
    private List<Error> errors;

    public BusinessValidationException(List<Error> errors) {
        this.errors=errors;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
    
}
