/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.common.exception;

import com.kubiko.common.exception.BusinessValidationResponse;
import com.kubiko.common.exception.Error;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *
 * @author abel
 */
@RestControllerAdvice
public class ControllerExceptionHandler {
    
    @ExceptionHandler(RollbackException.class)
    ResponseEntity<BusinessValidationResponse> exceptionHandlerRollbackException(RollbackException e) {
        final String cause = e.getCause().toString();
        List<Error> errors = new ArrayList<>();
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        
        if(cause.indexOf("ConstraintViolationImpl")!=-1) {
            status = HttpStatus.BAD_REQUEST;
            final String searchWord = "{interpolatedMessage='";
            final int start = cause.indexOf(searchWord)+searchWord.length();
            final int end = cause.indexOf("'", start);
            final String errMsg = cause.substring(start, end);
            errors.add(new Error("", errMsg));
        } else throw e;
        BusinessValidationResponse r = new BusinessValidationResponse(errors);
        return new ResponseEntity<>(r, status);
    }
    
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(EntityExistsException.class)
//    BusinessValidationResponse exceptionHandlerEntityExistsException(EntityExistsException e) {
//        List<Error> errors = new ArrayList<>();
//        if(e.getLocalizedMessage().indexOf("EntidadeCAESec")!=-1)
//            errors.add(new Error("cae", "Existem caes duplicados em caes secundários!"));
//        else
//            errors.add(new Error(null, e.getLocalizedMessage()));
//        BusinessValidationResponse r = new BusinessValidationResponse(errors);
//        return r;
//    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    BusinessValidationResponse exceptionHandler(ConstraintViolationException e) {
        List<Error> errors = new ArrayList<>();

        e.getConstraintViolations().forEach(val->{
            String field=null;
            
            for(Path.Node item: val.getPropertyPath()) {
                field = item.toString();
            }
            
            errors.add(new Error(field, val.getMessage(), (String) val.getInvalidValue(),
                    "", val.getLeafBean().getClass().getSimpleName()));

        });
        return new BusinessValidationResponse(errors);
    }
}
