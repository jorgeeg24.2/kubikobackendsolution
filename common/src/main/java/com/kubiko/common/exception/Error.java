/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.common.exception;

/**
 *
 * @author abel
 */
public class Error {

    private String field;
    private String defaultMessage;
    private String rejectedValue;
    private String code;
    private String objectName;

    public Error(String field, String defaultMessage) {
        this.defaultMessage = defaultMessage;
        this.field = field;
    }
    public Error(String field, String defaultMessage, String rejectedValue,
            String code, String objectName) {
        this.defaultMessage = defaultMessage;
        this.field = field;
        this.rejectedValue=rejectedValue;
        this.code=code;
        this.objectName=objectName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getRejectedValue() {
        return rejectedValue;
    }

    public void setRejectedValue(String rejectedValue) {
        this.rejectedValue = rejectedValue;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    public void setDefaultMessage(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }
}
