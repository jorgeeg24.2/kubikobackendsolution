//package com.kubiko.common.dao;
//
//import com.ine.common.models.Debug;
//import com.ine.common.models.QDebug;
//import com.querydsl.core.types.dsl.StringExpression;
//import com.querydsl.core.types.dsl.StringPath;
//import java.util.Optional;
//import org.springframework.data.querydsl.QuerydslPredicateExecutor;
//import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
//import org.springframework.data.querydsl.binding.QuerydslBindings;
//import org.springframework.data.querydsl.binding.SingleValueBinding;
//import org.springframework.data.repository.PagingAndSortingRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface IDebugRepository extends PagingAndSortingRepository<Debug, Long>, QuerydslPredicateExecutor<Debug>, QuerydslBinderCustomizer<QDebug> {
//
//    @Override
//    default public void customize(QuerydslBindings bindings, QDebug entity) {
//        bindings.bind(String.class)
//                .first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
//        bindings.bind(entity.user).all((path, value) -> Optional.of(path.in(value)));
//    }
//}
