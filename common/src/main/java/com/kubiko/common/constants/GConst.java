/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.common.constants;

/**
 *
 * @author abel
 */
public class GConst {
    
    public static final String SYS_USER = "SYSTEM";
    public static final String PENDING = "P";

    public static final String ENTITY_TYPE_COLLECTIVE = "C";
    public static final String ENTITY_TYPE_SINGLE = "I";
    public static final String ENTITY_TYPE_GROUP = "G";
    
    public static final String SENT = "ENV";
    public static final String FAIL = "FAL";
    public static final String VALIDATED = "V";

    public static final String ENTITY = "ENTIDADE";
    
    public static final String CONTACT_TYPE_EMAIL = "EMAIL";
    public static final String CONTACT_TYPE_MOBILE = "TELEMOVEL";
    public static final String CONTACT_TYPE_TEL = "TELEFONE";
    public static final String CONTACT_TYPE_FAX = "FAX";
    
    public static final String NOTIFICATION_TYPE_EMAIL = "EMAIL";
    public static final String NOTIFICATION_TYPE_SMS = "SMS";
    public static final String NOTIFICATION_TYPE_APP = "APLICACAO";
    
    public static final String DATE_TIME_FORMAT = "dd-mm-yyyy hh:mm:ss";
    public static final String DATE_FORMAT = "dd-mm-yyyy";
    
    // UNIVERSE ETAPAS
    public static final String KAFKA_UNIV_RESPONSE_SUCCESS = "SUCCESS";
    public static final String KAFKA_UNIV_RESPONSE_ERROR = "ERROR";
    public static final String UNIV_ETAPAS_PENDENTE = PENDING;
    public static final String UNIV_ETAPAS_CONCLUIDO = "CON";
    public static final String UNIV_ETAPAS_AGUARDANDO = "AGRD";
    public static final String UNIV_ETAPAS_ERRO_UNIVERSO = "ERR";
}
