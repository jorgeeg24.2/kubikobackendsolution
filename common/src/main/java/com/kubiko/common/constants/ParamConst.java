/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubiko.common.constants;

/**
 *
 * @author abel
 */
public final class ParamConst {
    
    private ParamConst(){}
    
    public static final String PARAM_PAISES="PARAM_PAISES";
    public static final String PARAM_CAE="PARAM_CAE";
    public static final String PARAM_TITULO_ACADEMICO="PARAM_TITULO_ACADEMICO";
    public static final String PARAM_SITUACAO="PARAM_SITUACAO";
    public static final String PARAM_SECTOR="PARAM_SECTOR";
    public static final String PARAM_COMUNAS="PARAM_COMUNAS";
    public static final String PARAM_TIPO_ALUGUER="PARAM_TIPO_ALUGUER";
    public static final String PARAM_TIPO_COMPRA="PARAM_TIPO_COMPRA";
    public static final String PARAM_TIPO_DISTANCIA="PARAM_TIPO_DISTANCIA";
    public static final String PARAM_TIPO_VIZINHANCA="PARAM_TIPO_VIZINHANCA=";
    public static final String PARAM_TIPO_PROPRIEDADE="PARAM_TIPO_PROPRIEDADE";
    public static final String PARAM_TIPO_CONVENIENCIA="PARAM_TIPO_CONVENIENCIA";
    public static final String PARAM_TIPO_CASA="PARAM_TIPO_CASA";

    public static final int OP_CREATE=0;
    public static final int OP_UPDATE=1;
    public static final int OP_DELETE=2;
    
}
