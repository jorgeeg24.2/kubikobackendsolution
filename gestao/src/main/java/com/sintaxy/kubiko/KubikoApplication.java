package com.sintaxy.kubiko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KubikoApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubikoApplication.class, args);
	}

}
